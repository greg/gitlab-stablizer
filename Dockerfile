FROM ubuntu:20.04

RUN apt-get update && apt-get -y install curl
RUN curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | bash
COPY stabilizer.sh .
