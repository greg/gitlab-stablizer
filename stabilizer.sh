#!/usr/bin/env bash

LATEST_MAJOR=$(apt list gitlab-ee -a | awk '{print $2}' | sort | awk -F '.' '{print $1}' | tail -n 1)
LATEST_MINOR=$(apt list gitlab-ee -a | awk '{print $2}' | sort | awk -F '.' '{print $2}' | tail -n 1)
PREVIOUS_MINOR="$((LATEST_MINOR-1))"
STABLE_RELEASE="$LATEST_MAJOR.$PREVIOUS_MINOR"
STABLE_APT_RELEASE=$(apt list gitlab-ee -a | grep $STABLE_RELEASE | awk '{print $2}' | sort | tail -n 1)

echo $LATEST_MAJOR
echo $LATEST_MINOR
echo $PREVIOUS_MINOR
echo $LATEST_MAJOR.$PREVIOUS_MINOR
echo $STABLE_RELEASE
apt install -y gitlab-ee=$STABLE_APT_RELEASE